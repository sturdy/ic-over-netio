#ifndef __UTILITIES__
#define __UTILITIES__

#include <vector>
#include <string>
#include <cstdint>
#include <iostream>
#include <sstream>

// check if a file exist
bool isFile(std::string filename);

// convert string of size 1 to an uint8(4 least significant bits)
// convert string of size 2 to an uint8
uint8_t strTouint8(std::string str);

#define HexDec(X) std::hex << "0x" << X << "(" << std::dec << X << ")"

// template<typename T> std::string HexDec(T X){
//   std::stringstream ss;
//   std::cout << X << std::endl;
//   ss << std::hex << X << "(" << std::dec << X << ")";
//   return ss.str();
// }

#define Hex(X)  std::hex << "0x" << X << std::dec

#endif
