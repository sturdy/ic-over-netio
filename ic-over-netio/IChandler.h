

#ifndef __IC_HANDLER__
#define __IC_HANDLER__

#include <string>
#include <vector>
#include <cstdint>

#include "connectionHandler.h"

#ifdef FELIX_BUILD
#include <netio/netio.hpp>
#else
#include <netio.hpp>
#endif
#include <felixbase/client.hpp>

class IChandler
{
  public:
    enum class DeviceType {
      GBTx,
      lpGBT
    };

    IChandler() = delete;
    IChandler(std::string flxHost, unsigned int portToGBTx, unsigned int portFromGBTx, uint32_t elinkID, bool resubscribeValue = false, DeviceType icDevice = DeviceType::GBTx);
    ~IChandler();

    void setMaxRetries(int);
    void sendCfg(std::string);
    // ATTENTION! those below will be useful for interfacing
    // will be useful for outside interfacing
    void sendCfg(const std::vector<uint8_t> &);
    // send to register(key)
    //   all the values in vector(item)
    //   e.g. { 0 => { '0xff', '0x00' }, 1 => { '0x02' } } means send to register 0 0xff, then 0x00; send to register 1, 0x02
    //   the order of register follows (auto & reg: map)
    void sendRegs(std::map<uint16_t, std::vector<uint8_t>>);
    // bool isConnected();

    std::vector<uint8_t>                      readCfg();
    // takes a vector of address 
    // returns a vector of pair<addr, val>
    std::vector<std::pair<uint16_t, uint8_t>> readRegs(std::vector<uint16_t>);

  private:
    // prepare the netio frame with proper register address and data(if write)
    std::vector<uint8_t> prepareICNetioFrame(bool read, uint16_t startAddr, const std::vector<uint8_t>& data);

    // newly added function, called by interfaces
    std::vector<unsigned char> prepareNetioFrame(bool read, uint16_t startAddr, const std::vector<uint8_t>& data);


    ConnectionHandler& m_connHandler;
    int m_maxRetries;
    bool m_resubscribe;
    std::string m_flxHost;
    unsigned int m_portToGBTx;
    unsigned int m_portFromGBTx;
    uint32_t m_elinkID;
    const DeviceType m_icDevice;   ///< Type of device that this IChandler works with
    const std::uint8_t m_i2cAddr;      ///< I2C address this IChandler will prepare frames for

    // void resubscribe();

  protected:

  public:
    constexpr static std::size_t NUM_GBTX_READABLE_REGISTERS{436}; ///< The GBTx has 436 total registers

    constexpr static std::size_t NUM_BYTES_NETIO_REPLY_HEADER{8}; ///< FELIX header in the  netio frame is 8 bytes
    constexpr static std::size_t NUM_BYTES_IC_HEADER{7};          ///< First 7 bytes of IC reply are header
    constexpr static std::size_t NUM_PARITY_BYTES_IC_TRAILER{1};  ///< Last byte of IC reply is parity word
    constexpr static std::size_t FIRST_IC_PAYLOAD_BYTE{NUM_BYTES_NETIO_REPLY_HEADER + NUM_BYTES_IC_HEADER};
    constexpr static std::uint8_t I2C_ADDRESS_GBTX{0x1};   ///< I2C address for GBTx
    constexpr static std::uint8_t I2C_ADDRESS_LPGBT{0x7f}; ///< I2C address for lpGBT
};

#endif
