#ifndef __CONNECTION_HANDLER__
#define __CONNECTION_HANDLER__

#include <string>
#include <vector>
#include <map>
#include <cstdint>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>

#include "ic-over-netio/utilities.h"

#ifdef FELIX_BUILD
#include <netio/netio.hpp>
#else
#include <netio.hpp>
#endif

#ifndef USE_SPDLOG
#include <ers/ers.h>
ERS_DECLARE_ISSUE(ion,
    SocketNotConnected,
    "Trying to send a message through a request socket that is not connected: " << host << ", "
  << HexDec(port),
    ((std::string) host)
    ((unsigned int ) port)
    )


ERS_DECLARE_ISSUE(ion,
    SocketNotSubscribed,
    "Trying to send a message but the reply socket is not subscribed and listening for the reply ({:s}, {:#04x}, {:#04x})" << host << ","
    << HexDec(port) << ", "
    << HexDec(link),
    ((std::string) host)
    ((unsigned int ) port)
    ((unsigned int ) link)
    )

ERS_DECLARE_ISSUE(ion,
    NoReply,
    "No reply is coming from E-link: " << what,
    ((std::string) what)
    )

ERS_DECLARE_ISSUE(ion,
    NetioForever,
    "From netio run_forever(): " << what,
    ((std::string) what)
    )
#endif

#include <felixbase/client.hpp>

class ICSocket;
class ICRequestSocket;
class ICReplySocket;

// a structure to save configuration for a connection
struct ConnectionInfo {
  std::string flxHost;
  unsigned int portToGBTx;
  unsigned int portFromGBTx;
  uint32_t elinkID;

  std::shared_ptr<ICRequestSocket> requestSocket;
  std::shared_ptr<ICReplySocket> replySocket;
};

class ICSocket
{
public:
  ICSocket() = delete;
  ICSocket(netio::context* context, std::string flxHost) :
    m_netioContext{context},
    m_flxHost{std::move(flxHost)}
  {}
  virtual ~ICSocket() = default;

  // Delete Copy and Assignment
  ICSocket(const ICSocket&) = delete;
  ICSocket& operator=(const ICSocket&) = delete;

  virtual void free() = 0;
  virtual void connect() = 0;
  void reconnect()
  {
    free();
    connect();
  };

  std::string flxHost() const { return m_flxHost; };

private:
protected:
  netio::context* m_netioContext;
  std::string m_flxHost;
};

class ICRequestSocket : public ICSocket
{
public:
  ICRequestSocket() = delete;
  ICRequestSocket(netio::context*, std::string, unsigned int);
  ~ICRequestSocket();

  // Delete Copy and Assignment
  ICRequestSocket(const ICRequestSocket&) = delete;
  ICRequestSocket& operator=(const ICRequestSocket&) = delete;

  void free() override;
  void connect() override;

  bool connected() const { return m_connected; };

  // used by user
  void sendNetioMessage(std::vector<unsigned char>&,
                        std::shared_ptr<ICReplySocket>,
                        int maxAttempts = 3,
                        bool resubscribeOnRetry = false);

  unsigned int portToGBTx() const { return m_portToGBTx; };

private:
  unsigned int m_portToGBTx;

  netio::low_latency_send_socket m_netioRequestSocket;
  bool m_connected{false};

protected:
};

class ICReplySocket : public ICSocket
{
public:
  ICReplySocket() = delete;
  ICReplySocket(netio::context*, std::string, unsigned int, uint32_t);
  ~ICReplySocket();

  // Delete Copy and Assignment
  ICReplySocket(const ICReplySocket&) = delete;
  ICReplySocket& operator=(const ICReplySocket&) = delete;

  void free() override;
  void connect() override;

  void replyHandler(netio::endpoint&, netio::message&);

  bool subscribed() const { return m_subscribed; };

  void resetReply() { m_hasReply = false; };
  bool hasReply() const { return m_hasReply; };
  // used by user
  std::vector<uint8_t> getReply() const { return m_hasReply ? m_reply : std::vector<std::uint8_t>{}; };

  std::mutex& mtx() { return m_mtx; };
  std::condition_variable& condVar() { return m_condVar; };

  unsigned int portFromGBTx() const { return m_portFromGBTx; };
  uint32_t elinkID() const { return m_elinkID; };

private:
  unsigned int m_portFromGBTx;
  uint32_t m_elinkID;

  netio::low_latency_subscribe_socket m_netioReplySocket;
  bool m_subscribed{false};

  std::mutex m_mtx;
  std::condition_variable m_condVar;

  // Set variable as volatile because it is being changed in one thread and accessed in another
  volatile bool m_hasReply{false};
  std::vector<uint8_t> m_reply{};

protected:
};

class ConnectionHandler
{
public:
  ConnectionHandler();
  ~ConnectionHandler();

  /**
   * \brief Get a singleton instance of the ConnectionHandler
   *
   * \returns the ConnectionHandler singleton instance
   */
  static ConnectionHandler& getInstance();

  /**
   * Establish an IC e-link connection to the GBTx through creating an ICSocket
   *
   * \param flxHost Hostname of the FELIX
   * \param portToGBTx felixcore port in the direction to the GBTx (from-host)
   * \param portFromGBTx felixcore port in the direction to the GBTx (to-host)
   * \param elinkID e-link ID for the IC channel
   *
   * \returns a ConnectionInfo containing request and reply sockets
  */
  ConnectionInfo getConnection(const std::string&, unsigned int, unsigned int, uint32_t);

private:
  /**
   * \brief Allocate the netio thread
   */
  void netioMainThread();

  /**
   * \brief Closes the connection by stopping the event loop and joining the netio threads
   */
  void freeConnections();

private:
  netio::context m_netioContext;
  std::thread m_netioThread;

  std::map<std::string, std::shared_ptr<ICRequestSocket>> m_requestSockets;
  std::map<std::string, std::shared_ptr<ICReplySocket>> m_replySockets;

  /**
   * \brief Calls free on all the sockets in m_replySockets
   */
  void freeReplySockets();

  /**
   * \brief Calls free on all the sockets in m_requestSockets
   */
  void freeRequestSockets();

protected:
};

#endif
