#ifndef _NSW_IC_LOGGING_
#define _NSW_IC_LOGGING_

#include <string>

#ifdef USE_SPDLOG
#define SPDLOG_FMT_EXTERNAL_HO
#include <spdlog/spdlog.h>
extern std::shared_ptr<spdlog::logger> console;
#else

#include "ers/ers.h"
ERS_DECLARE_ISSUE(ion,
    FileNotExist,
    "The given file " << fileName << " does not exist.",
    ((std::string) fileName)
    )

ERS_DECLARE_ISSUE(ion,
    ConfigFile,
    errmsg,
    ((std::string) errmsg)
    )

ERS_DECLARE_ISSUE(ion,
    Lazy,
    errmsg,
    ((std::string) errmsg)
    )

#endif // FELIX_BUILD
#endif // Hdr Guard
