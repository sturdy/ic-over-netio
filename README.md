# General Information
Project has started as of 31/07/2019

see https://its.cern.ch/jira/browse/ATLNSWDAQ-51

also https://espace.cern.ch/GBT-Project/GBTX/Manuals/gbtxManual.pdf

# Compilation
## Compilation using CMakeLists.txt_Old

First you need to change the name to CMakeLists.txt. Then there's two ways to compile it.

1. Together with felix software. 
  * The [Felix software](https://gitlab.cern.ch/atlas-tdaq-felix/software) should be downloaded.
  * `git clone` this module into the felix software directory
  * follow the procedure of felix-software for compilation, that includes:  
    * source tdaq_cmake that comes from  (must always be done at start of session): `source software/cmake_tdaq/bin/setup.sh x86_64-centos7-gcc8-opt`)
    * cmake_config
    * do `make` in `x86_64-centos7-gcc8-opt`

2. upon an rpm, but using external software from felix.
  * the following felix software need to be partially downloaded: external/spdlog, external/json, tdaq_cmake.
  * `export FELIX_ROOT="XXX/opt/felix/"`, which is rpm installed area. Notice that rpm can be extracted on any machine, so XXX is the path to it.
  * create a build directory, go into it
  * `cmake -DSOFTWARE_PATH = < absolute path to FELIX SOFTWARE PATH > <absolute path to  path to ic-over-netio >. Here at least < FELIX SOFTWARE PATH >/external should exist.

## Compilation using tdaq release 
Another solution is to compile together with nswdaq release https://gitlab.cern.ch/atlas-muon-nsw-daq/nswdaq.

clone the nswdaq release, branch `nswdaq-00-01-04-ICoverNetio`

```
mkdir felix-rpm
cd felix-rpm
rpm2cpio ./<felix>.rpm | cpio -idmv
```

Then compile according to nswdaq README.md


### use flx release rpm 
Now you can compile it as part of nswdaq release, but need external softwares, linked in the nswdaq directory in the nswdaq directory, this is not 
implemented yet https://gitlab.cern.ch/rowang/ic-over-netio/-/issues/1

## Installing FLX software
Follow instructions on:
https://gitlab.cern.ch/atlas-tdaq-felix/software

## Acknowledgments
Special thanks to OPC-UA and FELIX developers.

Using code from https://gitlab.cern.ch/atlas-dcs-common-software/
and https://gitlab.cern.ch/atlas-tdaq-felix/ftools

