#include "ic-over-netio/logging.h"

#ifdef USE_SPDLOG
#include <spdlog/sinks/stdout_color_sinks.h>
std::shared_ptr<spdlog::logger> console = spdlog::stdout_color_mt("console");
#endif

