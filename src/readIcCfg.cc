#include "ic-over-netio/readIcCfg.h"
#include "ic-over-netio/logging.h"
#include "ic-over-netio/utilities.h"

#include <fstream>
#include <cstdlib>
#include <stdexcept>
#include <cstdint>

// read a file full of registers and convert it to an m_data
readICCfg::readICCfg(std::string cfg_file):
  m_cfg_file_name(cfg_file)
{
  if(!isFile(cfg_file))
  {
#ifdef USE_SPDLOG
    console->error("The given file ({:s}) does not exist", m_cfg_file_name);
    throw std::logic_error("File does not exist");
#else
    ion::FileNotExist issue(ERS_HERE, m_cfg_file_name);
    ers::error(issue);
    throw issue;
#endif
  }

  m_data.reserve(1024);
  std::ifstream inFile(m_cfg_file_name);

  std::string line;
  m_isOK = true;

  try{
  while(std::getline(inFile, line))
  {
    if(line.size() > 2 || line.size() == 0)
    {
#ifdef USE_SPDLOG
      console->error("File {:s} does not have the correct format", m_cfg_file_name);
#else
      ion::ConfigFile issue(ERS_HERE, "File " + m_cfg_file_name + " does not have the correct format");
      ers::error(issue);
#endif
      m_isOK = false;
      break;
    }

    m_data.push_back(strTouint8(line));

    if(m_data.size() > 1023)
    {
#ifdef USE_SPDLOG
      console->error("File {:s} has too many lines", m_cfg_file_name);
#else
      ion::ConfigFile issue(ERS_HERE, "File " + m_cfg_file_name + " has too many lines");
      ers::error(issue);
#endif
      m_isOK = false;
      break;
    }
  }
  }
  catch(std::logic_error& e)
  {
#ifdef USE_SPDLOG
    console->error("File {:s} does not have the correct format, message: {:s}", m_cfg_file_name, e.what());
#else
    ion::ConfigFile issue(ERS_HERE, "File " + m_cfg_file_name + " does not have the correct format, message: " + e.what());
    ers::error(issue);
#endif
    m_isOK = false;
  }
}
