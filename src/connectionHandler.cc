#include "ic-over-netio/connectionHandler.h"

#include <chrono>
#include <cstdint>
#include <exception>
#include <sstream>

#include <fmt/core.h>

#ifndef USE_SPDLOG
#include <ers/ers.h>
#endif

#include "ic-over-netio/logging.h"
#include "ic-over-netio/utilities.h"

using namespace std::chrono_literals;

const std::chrono::milliseconds connectWaitTime = 200ms;

ICRequestSocket::ICRequestSocket(netio::context* context,
                                 std::string flxHost,
                                 unsigned int portToGBTx) :
  ICSocket{context, std::move(flxHost)},
  m_portToGBTx{portToGBTx},
  m_netioRequestSocket{m_netioContext}
{
  connect();
}

ICRequestSocket::~ICRequestSocket()
{
  free();
}

void ICRequestSocket::connect()
{
  if (m_connected) {
    return;
  }

  m_netioRequestSocket.connect(netio::endpoint(m_flxHost, m_portToGBTx));

  if (m_netioRequestSocket.is_open()) { // test the connection
    m_connected = true;
  } else {
    throw std::runtime_error("ICRequestSocket::connect() Unable to connect to FELIX endpoint. The felixcore application seems to be down.");
  }
}

void ICRequestSocket::free()
{
  if (!m_connected) {
    return;
  }

  m_netioRequestSocket.disconnect();
  m_connected = false;
}

void ICRequestSocket::sendNetioMessage(std::vector<unsigned char>& netioFrame,
                                       std::shared_ptr<ICReplySocket> replySocket,
                                       int maxAttempts,
                                       bool resubscribeOnRetry)
{
  if (!connected()) {
    const auto msg = fmt::format(
      "Trying to send a message through a request socket that is not connected ({:s}, {:#04x})",
      m_flxHost,
      m_portToGBTx);
#ifdef USE_SPDLOG
    console->error(msg);
    throw std::runtime_error(msg);
#else
    ion::SocketNotConnected issue(ERS_HERE, m_flxHost, m_portToGBTx);
    ers::error(issue);
    throw issue;
#endif
  }

  if (!replySocket->subscribed()) {
#ifdef USE_SPDLOG
    const auto msg = fmt::format("Trying to send a message but the reply socket is not subscribed "
                                 "and listening for the reply ({:s}, {:#04x}, {:#04x})",
                                 replySocket->flxHost(),
                                 replySocket->portFromGBTx(),
                                 replySocket->elinkID());
    console->error(msg);
    throw std::runtime_error(msg);
#else
    ion::SocketNotSubscribed issue(ERS_HERE, replySocket->flxHost(), replySocket->portFromGBTx(), replySocket->elinkID());
    ers::error(issue);
    throw issue;
#endif
  }

  int attempt = 0;
  bool retry  = false;

  do {
    retry       = false;
    replySocket->resetReply();
    if (attempt > 0) {
      const auto msg = fmt::format("Attempt {0} for E-link {1:x} (1:d)", attempt + 1, replySocket->elinkID());
#ifdef USE_SPDLOG
      // console->debug(msg);
#else
      ERS_DEBUG(1, msg);
#endif
    }

    netio::message message(&netioFrame[0], netioFrame.size());
    m_netioRequestSocket.send(message);

    // Start a lock block so it synchronizes with the thread processing the replies
    {
      std::unique_lock<std::mutex> lockParameter(replySocket->mtx());

      // Wait for the other thread to notify that there is a reply or timeout
      if (!(replySocket->condVar().wait_for(
            lockParameter, 3000ms, [&] { return replySocket->hasReply(); }))) {
        const auto msg = fmt::format("Reply hasn't come for E-link {0:x} ({0:d}) after 3000 ms", replySocket->elinkID());
#ifdef USE_SPDLOG
        console->debug(msg);
#else
        ERS_DEBUG(1, msg);
#endif
        if (++attempt < maxAttempts) {
          if (resubscribeOnRetry) { // TODO: Is order important?
            replySocket->free();
            reconnect();
            replySocket->connect();
            std::this_thread::sleep_for(connectWaitTime);
          }

          retry = true;
        }
      }
    }

  } while(retry);

  if (!replySocket->hasReply()) {
    const auto msg = fmt::format("No reply is coming from E-link: {0:x} ({0:d})", replySocket->elinkID());
#ifdef USE_SPDLOG
    console->warn(msg);
#else
    ion::NoReply issue(ERS_HERE, msg);
    ers::warning(issue);
#endif
  }
}

ICReplySocket::ICReplySocket(netio::context* context,
                             std::string flxHost,
                             unsigned int portFromGBTx,
                             uint32_t elinkID) :
  ICSocket{context, std::move(flxHost)},
  m_portFromGBTx{portFromGBTx},
  m_elinkID{elinkID},
  m_netioReplySocket{m_netioContext, [this](netio::endpoint& endpoint, netio::message& message) {
                       this->replyHandler(endpoint, message);
                     }}
{
  connect();
}

ICReplySocket::~ICReplySocket()
{
  free();
}

void ICReplySocket::connect() {
  if (m_subscribed) {
    return;
  }

  m_netioReplySocket.subscribe(m_elinkID, netio::endpoint(m_flxHost, m_portFromGBTx));
  m_subscribed = true;
}

void ICReplySocket::free() {
  if (!m_subscribed) {
    return;
  }

  m_netioReplySocket.unsubscribe(m_elinkID, netio::endpoint(m_flxHost, m_portFromGBTx));
  m_subscribed = false;
}

void ICReplySocket::replyHandler(netio::endpoint& endpt, netio::message& message) {
  const auto msg1 = fmt::format("replyHandler mem: {} {:#018x}", fmt::ptr(this), reinterpret_cast<uintptr_t>(this));
  const auto msg2 = fmt::format("endpoint: {:s}:{:d}", endpt.address(), endpt.port());
#ifdef USE_SPDLOG
  console->trace(msg1);
  console->trace(msg2);
#else
  ERS_DEBUG(2, msg1);
  ERS_DEBUG(2, msg2);
#endif

  // TODO: Reduce the number of locks, put it all in one or maybe two, there is no need for 3

  { // Lock in order to clear the variable holding the reply
    std::unique_lock<std::mutex> lockParameter(m_mtx);
    m_reply.clear();
  }

  // Retrieve the reply from the message and put into a temporary variable
  const std::vector<uint8_t> netioFrame(message.data_copy());

  { // Lock in order to fill the reply variable with the reply message
    std::unique_lock<std::mutex> lockParameter(m_mtx);
    m_reply = netioFrame;
  }

  // TODO: Below just for debugging, delete it later
  // (Maybe not needed to delete because it is using a trace message)
  std::string out;
  for (const auto& byte : netioFrame) {
    out += fmt::format("{:02X} ", byte);
  }
  const auto msg3 = fmt::format("Netio reply from {:s}:{:d}, elink {:#04x}:\n{:s}", m_flxHost, m_portFromGBTx, m_elinkID, out);
#ifdef USE_SPDLOG
  console->trace(msg3);
#else
  ERS_DEBUG(2, msg3);
#endif

  { // Lock in order to set that a reply is available and notify the condition variable
    std::unique_lock<std::mutex> lockParameter(m_mtx);
    m_hasReply = true;
    m_condVar.notify_one();
  }
}

ConnectionHandler::ConnectionHandler() :
  m_netioContext{"posix"},
  m_netioThread{&ConnectionHandler::netioMainThread, this}
{}

ConnectionHandler::~ConnectionHandler()
{
  freeConnections();

  m_netioContext.event_loop()->stop();
  m_netioThread.join();
}

ConnectionHandler& ConnectionHandler::getInstance()
{
  static ConnectionHandler instance;
  return instance;
}

// allocate thread, only called in constructor
void ConnectionHandler::netioMainThread() {
  try {
    m_netioContext.event_loop()->run_forever();
  } catch (const std::exception& e) {
    const auto errmsg = fmt::format("From netio run_forever(): {:s}", e.what());
#ifdef USE_SPDLOG
    console->error(errmsg);
#else
    ion::NetioForever issue(ERS_HERE, errmsg);
    ers::error(issue);
#endif
    // TODO not throwing here?
  }
}

// helper, only called in destructor
void ConnectionHandler::freeConnections()
{
  freeReplySockets();
  freeRequestSockets();
}

// helper, only called in freeConnections()
void ConnectionHandler::freeReplySockets()
{
  for (auto& socket : m_replySockets) {
    try {
      socket.second->free();
    } catch(...) {
      // FIXME Catch appropriate exception
      const auto errmsg = fmt::format("Crashed port : {:d}, elink {:#07x}", socket.second->portFromGBTx(), socket.second->elinkID());
#ifdef USE_SPDLOG
      console->trace(errmsg);
#else
      ERS_DEBUG(2, errmsg);
#endif
    }
  }
}

// helper, only called in freeConnections()
void ConnectionHandler::freeRequestSockets()
{
  for (auto& socket : m_requestSockets) {
    socket.second->free();
  }
}

// establish connection through creating ICSocket
// return a ConnectionInfo containing request and reply sockets
ConnectionInfo ConnectionHandler::getConnection(const std::string& flxHost,
                                                const unsigned int portToGBTx,
                                                const unsigned int portFromGBTx,
                                                const uint32_t elinkID)
{
  bool createdSocket = false;
  ConnectionInfo retVal{
    .flxHost{flxHost},
    .portToGBTx{portToGBTx},
    .portFromGBTx{portFromGBTx},
    .elinkID{elinkID},
  };

  const auto requestSocketStr = fmt::format("{:s}-{:#04x}", flxHost, portToGBTx);
  const auto replySocketStr   = fmt::format("{:s}-{:#04x}-{:#04x}", flxHost, portFromGBTx, elinkID);

  auto requestSock = m_requestSockets.find(requestSocketStr);
  if (requestSock != m_requestSockets.end()) {
    retVal.requestSocket = requestSock->second;
  } else {
    createdSocket = true;

    auto tmp = std::make_shared<ICRequestSocket>(&m_netioContext, flxHost, portToGBTx);

    m_requestSockets[requestSocketStr] = tmp;
    retVal.requestSocket = tmp;
  }

  auto replySock = m_replySockets.find(replySocketStr);
  if (replySock != m_replySockets.end()) {
    retVal.replySocket = replySock->second;
  } else {
    createdSocket = true;

    auto tmp = std::make_shared<ICReplySocket>(&m_netioContext, flxHost, portFromGBTx, elinkID);

    m_replySockets[replySocketStr] = tmp;
    retVal.replySocket = tmp;
  }

  // if socket is created, wait for a while in this thread
  if (createdSocket) {
    std::this_thread::sleep_for(connectWaitTime);
  }

  return retVal;
}
