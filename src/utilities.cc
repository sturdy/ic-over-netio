#include "ic-over-netio/utilities.h"

#include <sys/stat.h>
#include <cstdint>
#include <stdexcept>

bool isFile(std::string fileName)
{
  struct stat buffer;
  return (stat(fileName.c_str(), &buffer) == 0);
}

uint8_t strTouint8(std::string str)
{
  if(str.size() > 2 || str.size() == 0)
    throw std::logic_error("\"" + str + "\" is not a valid uint_8");

  uint8_t retVal = 0;

  for(size_t i = 0; i < str.size(); ++i)
  {
    retVal = retVal << 4;
    switch(str[i])
    {
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        retVal += static_cast<uint8_t>(str[i] - '0');
        break;
      case 'a':
      case 'b':
      case 'c':
      case 'd':
      case 'e':
      case 'f':
        retVal += static_cast<uint8_t>(str[i] - 'a') + 10;
        break;
      default:
        throw std::out_of_range("The value " + str + " is  not valid hexadecimal");
    }
  }

  return retVal;
}

