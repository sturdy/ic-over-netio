#include "ic-over-netio/IChandler.h"

#include <chrono>
#include <exception>

#ifndef USE_SPDLOG
#include <ers/ers.h>
#endif

#include "ic-over-netio/logging.h"
#include "ic-over-netio/utilities.h"
#include "ic-over-netio/readIcCfg.h"

#include <fmt/format.h>

using namespace std::chrono_literals;

IChandler::IChandler(std::string flxHost,
                     const unsigned int portToGBTx,
                     const unsigned int portFromGBTx,
                     const uint32_t elinkID,
                     const bool resubscribeValue,
                     const DeviceType ic_device):
  m_connHandler{ConnectionHandler::getInstance()},
  m_maxRetries{2},
  m_resubscribe{resubscribeValue}, //set to true if you want to resubscribe upon failure to receive message
  m_flxHost{flxHost},
  m_portToGBTx{portToGBTx},
  m_portFromGBTx{portFromGBTx},
  m_elinkID{elinkID},
  m_icDevice{ic_device},
  m_i2cAddr{m_icDevice==DeviceType::GBTx?I2C_ADDRESS_GBTX:I2C_ADDRESS_LPGBT}
{
}

IChandler::~IChandler()
{
}


void IChandler::setMaxRetries(int val) {
  if(val >= 0)
    m_maxRetries = val;
  else
    m_maxRetries = 2;
}

void IChandler::sendCfg(std::string fileName) {
  readICCfg icCfg(fileName);
  sendCfg( icCfg.data() );
}

void IChandler::sendCfg(const std::vector<uint8_t> &data) {
  std::vector<unsigned char> netioFrame = prepareNetioFrame(false, 0, data);
  ConnectionInfo connInfo = m_connHandler.getConnection(m_flxHost, m_portToGBTx, m_portFromGBTx, m_elinkID);
  connInfo.requestSocket->sendNetioMessage(netioFrame, connInfo.replySocket, m_maxRetries+1, m_resubscribe);
}

void IChandler::sendRegs(std::map<uint16_t, std::vector<uint8_t>> regs) {
  ConnectionInfo connInfo = m_connHandler.getConnection(m_flxHost, m_portToGBTx, m_portFromGBTx, m_elinkID);
  for(auto& reg : regs) {
    std::vector<unsigned char> netioFrame = prepareNetioFrame(false, reg.first, reg.second);
    connInfo.requestSocket->sendNetioMessage(netioFrame, connInfo.replySocket, m_maxRetries+1, m_resubscribe);
  }
}

std::vector<uint8_t> IChandler::readCfg() {
  std::vector<uint8_t> retVal;
  std::vector<uint8_t> dummy(436);
  ConnectionInfo connInfo = m_connHandler.getConnection(m_flxHost, m_portToGBTx, m_portFromGBTx, m_elinkID);

  std::vector<unsigned char> netioFrame = prepareNetioFrame(true, 0, dummy);

  connInfo.requestSocket->sendNetioMessage(netioFrame, connInfo.replySocket, m_maxRetries+1, m_resubscribe);
  auto reply = connInfo.replySocket->getReply();

  // Remove FELIX and IC header words and parity trailer word
  for (std::size_t i{FIRST_IC_PAYLOAD_BYTE}; i < reply.size() - NUM_PARITY_BYTES_IC_TRAILER; ++i) {
    retVal.push_back(reply.at(i));
  }
  return retVal;
}

std::vector<std::pair<uint16_t, uint8_t>> IChandler::readRegs(std::vector<uint16_t> regs) {
  std::vector<std::pair<uint16_t, uint8_t>> retVal;
  ConnectionInfo connInfo = m_connHandler.getConnection(m_flxHost, m_portToGBTx, m_portFromGBTx, m_elinkID);

  const std::vector<uint8_t> dummy(1);

  for (const auto& reg : regs) {
    std::vector<unsigned char> netioFrame = prepareNetioFrame(true, reg, dummy);

    connInfo.requestSocket->sendNetioMessage(netioFrame, connInfo.replySocket, m_maxRetries+1, m_resubscribe);

    // TODO: is this finished? it seems we are not retrieving the replies with the register values
    // TODO: test this!
    // Remove Felix header and GBTx header
    auto reply = connInfo.replySocket->getReply();
    if (reply.size() > FIRST_IC_PAYLOAD_BYTE) {
      retVal.emplace_back(reg, reply.at(FIRST_IC_PAYLOAD_BYTE));
    }
  }

  return retVal;
}


// TODO: what to do with i2c config?
std::vector<uint8_t> IChandler::prepareICNetioFrame(const bool read, const uint16_t startAddr, const std::vector<uint8_t>& data) {
  std::vector<uint8_t> retVal = {};

  constexpr size_t header_size = 7;
  constexpr size_t footer_size = 2;

  // TODO(christos): Should we add the delimiters top and bottom?
  if(!read)
    retVal.reserve(header_size + data.size() + footer_size);
  else
    retVal.reserve(header_size + footer_size);

  retVal.push_back(0); // Delimiter
  retVal.push_back((m_i2cAddr << 1) + (read?0x1:0x0)); // GBTX I2C address and read/write bit
  retVal.push_back(1); // Command (not used in GBTX v1 + 2)
  retVal.push_back(data.size() & 0xFF); // Number of data bytes
  retVal.push_back((data.size() >> 8) & 0xFF);
  retVal.push_back(startAddr   & 0xFF); // Register (start) address
  retVal.push_back(startAddr >> 8);

  if(!read)
    for(auto& val: data)
      retVal.push_back(val);

  // For GBTx, skip first 2 bytes in parity check (see above)
  constexpr std::size_t NUM_PARTITY_BYTES_SKIP{2};

  uint8_t parity = 0;
  for(size_t i = NUM_PARTITY_BYTES_SKIP;i < retVal.size(); ++i)
    parity ^= retVal[i];
  retVal.push_back(parity);
  //retVal.push_back(0); // Delimiter

  return std::move(retVal);
}

std::vector<uint8_t> IChandler::prepareNetioFrame(const bool read, const uint16_t startAddr, const std::vector<uint8_t>& data) {
  auto icNetioFrame = prepareICNetioFrame(read, startAddr, data);
  felix::base::ToFELIXHeader header;
  std::vector<unsigned char> netioFrame ( sizeof(header) + icNetioFrame.size());

  header.length = netioFrame.size() - sizeof(header);
  header.reserved = 0;
  header.elinkid = m_elinkID;

  std::copy(
          reinterpret_cast<uint8_t*>(&header),
          reinterpret_cast<uint8_t*>(&header)+sizeof(header),
          netioFrame.begin() );

  memcpy(&netioFrame[sizeof header], icNetioFrame.data(), icNetioFrame.size());
  std::size_t i{0};
  for (const auto frameByte : netioFrame) {
    const auto msg = fmt::format("{0}: {1:#04x} ({1:d})", i++, frameByte);
#ifdef USE_SPDLOG
    console->trace(msg);
#else
    // this is extremely redundant, avoid easily calling it from NSWConfig
    ERS_DEBUG(5, msg);
#endif
  }

  return netioFrame;
}
