#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>

#include <fmt/core.h>

#include <cxxopts.hpp>

#ifndef USE_SPDLOG
#include <ers/ers.h>
#include <ers/Configuration.h>
#endif

#include "ic-over-netio/logging.h"
#include "ic-over-netio/readIcCfg.h"
#include "ic-over-netio/utilities.h"
#include "ic-over-netio/IChandler.h"

int main(int argc, char** argv)
{
  cxxopts::Options options(argv[0],
                           "A utilitiy to configure/interact with the IC channel over Netio");

  options.add_options()("h,help", "Print this help message", cxxopts::value<bool>())(
    "d,debug", "Enable debug (and verbose) output", cxxopts::value<bool>())(
    "t,trace", "Enable trace (and verbose) output", cxxopts::value<bool>());

  std::string felixHostname = "pcatlnswfelix01.cern.ch";
  unsigned int portToGBTx = 12340;
  unsigned int portFromGBTx = 12350;
  uint32_t elinkId = 0x3e;
  options.add_options("Configuration")(
    "f,felixHostname",
    fmt::format("The hostname of the machine the GBTx is connected to. Default: {:s}",
                felixHostname),
    cxxopts::value<std::string>())(
    "T,portToGBTx",
    fmt::format("The port to which to send data to the GBTx. Default {:d}", portToGBTx),
    cxxopts::value<uint>())(
    "r,portFromGBTx",
    fmt::format("The port from which to receive data from the GBTx. Default: {:d}", portFromGBTx),
    cxxopts::value<uint>())(
    "e,eLinkID",
    fmt::format("The elinkID with which to interact on the host machine. Receives hexadecimal "
                "(0xnnnn), ocatal (0nnnnnn) or decimal (nnnnn) Default: {:#04x}",
                elinkId),
    cxxopts::value<std::string>());

  bool read = false;
  std::vector<uint16_t> readAddresses = {};
  options.add_options("READ")("read", "Read and print the configuration", cxxopts::value<bool>())(
    "registers",
    "If specified, will define the addresses of the registers to be read. The registers can "
    "be passed as a list (eg: --registers 20,24,1,50) no spaces are allowed inside the list. The "
    "addresses are given in decimal",
    cxxopts::value<std::vector<int>>());

  std::string configFileName;
  std::map<uint16_t, std::vector<uint8_t>> addrValue;
  options.add_options("WRITE")(
    "c,config", "GBTx configuration file", cxxopts::value<std::string>())(
    "a,addresses",
    "Adresses and values to be written to. Specify the first address, then the length "
    "and then the value to be written into those addresses. Write more triplets for more addresses "
    "to "
    "be written to. (e.g: write to 102 3 sequential values of ff and 204 2 sequential values of c4 "
    "--addresses 102,3,ff,204,2,c4)"
    ". The values are assumed to be passed in hexadecimal and must be a single byte, the addresses "
    "are assumed to be in decimal.",
    cxxopts::value<std::vector<std::string>>());

  try {
    auto result = options.parse(argc, argv);
#ifdef USE_SPDLOG
    if (result.count("debug") > 0) {
      spdlog::set_level(spdlog::level::debug);
    }
    if (result.count("trace") > 0) {
      spdlog::set_level(spdlog::level::trace);
    }
#else
    ers::Configuration::instance().debug_level(0);
    if (result.count("debug") > 0) {
      ers::Configuration::instance().debug_level(1);
    }
    if (result.count("trace") > 0) {
      ers::Configuration::instance().debug_level(5);
    }
#endif

    if (result.count("help") > 0) {
      std::cout << options.help() << std::endl;
      return 0;
    }

    if (result.count("felixHostname") > 0) {
      if (result.count("felixHostname") == 1) {
        felixHostname = result["felixHostname"].as<std::string>();
      } else {
        const std::string msg = "Please define only one FELIX hostname";
#ifdef USE_SPDLOG
        console->error(msg);
#else
        ion::Lazy issue(ERS_HERE, msg);
        ers::error(issue);
#endif
        return 1;
      }
    }

    if (result.count("portToGBTx") > 0) {
      if (result.count("portToGBTx") == 1) {
        portToGBTx = result["portToGBTx"].as<uint>();
      } else {
        const std::string msg = "Please define only one port to the GBTx";
#ifdef USE_SPDLOG
        console->error(msg);
#else
        ion::Lazy issue(ERS_HERE, msg);
        ers::error(issue);
#endif
        return 1;
      }
    }

    if (result.count("portFromGBTx") > 0) {
      if (result.count("portFromGBTx") == 1) {
        portFromGBTx = result["portFromGBTx"].as<uint>();
      } else {
        const std::string msg = "Please define only one port to the GBTx";
#ifdef USE_SPDLOG
        console->error(msg);
#else
        ion::Lazy issue(ERS_HERE, msg);
        ers::error(issue);
#endif
        return 1;
      }
    }

    if (result.count("eLinkID") > 0) {
      if (result.count("eLinkID") == 1) {
        std::string tmp = result["eLinkID"].as<std::string>();
        try {
          elinkId = std::stoul(
            tmp, 0, 0);  // stoul automatically infers the base: 0 -> octal, 0x -> Hex, else decimal
          std::string tmp2;
          if (tmp[0] == '0') {
            if (tmp[1] == 'x') {
              tmp2 = fmt::format("{:#x}", elinkId);
            } else {
              if (tmp[1] == 'X') {
                tmp2 = fmt::format("{:#X}", elinkId);
              } else {
                tmp2 = fmt::format("{:#o}", elinkId);
              }
            }
          } else {
            tmp2 = fmt::format("{:d}", elinkId);
          }
          if (tmp != tmp2) {
            throw std::exception();
          }
        } catch (...) {
          const std::string msg = "Please give a valid e-link ID";
#ifdef USE_SPDLOG
          console->error(msg);
#else
          ion::Lazy issue(ERS_HERE, msg);
          ers::error(issue);
#endif
          std::cout << options.help() << std::endl;
          throw;
        }
      } else {
        const std::string msg = "Please define only one e-link ID";
#ifdef USE_SPDLOG
        console->error(msg);
#else
        ion::Lazy issue(ERS_HERE, msg);
        ers::error(issue);
#endif
        return 1;
      }
    }

    if (result.count("read") > 0) {
      read = true;
    }
    if (result.count("registers") > 0) {
      if (result.count("registers") > 1) {
        const std::string msg = "Please define only one list of registers to be read";
#ifdef USE_SPDLOG
        console->error(msg);
#else
        ion::Lazy issue(ERS_HERE, msg);
        ers::error(issue);
#endif
        return 1;
      }
      auto tmp = result["registers"].as<std::vector<int>>();
      for (auto& addr : tmp)
        readAddresses.push_back(addr);
    }

    if (result.count("config") > 0) {
      if (result.count("config") > 1) {
        const std::string msg = "Please define only one configuration file";
#ifdef USE_SPDLOG
        console->error(msg);
#else
        ion::Lazy issue(ERS_HERE, msg);
        ers::error(issue);
#endif
        return 1;
      }
      configFileName = result["config"].as<std::string>();
      if (!isFile(configFileName)) {
        const std::string msg = "Please define an existing configuration file";
#ifdef USE_SPDLOG
        console->error(msg);
#else
        ion::Lazy issue(ERS_HERE, msg);
        ers::error(issue);
#endif
        return 1;
      }
    }

    if (result.count("addresses") > 0) {
      if (result.count("addresses") > 1) {
        const std::string msg = "Please define only one set of addresses";
#ifdef USE_SPDLOG
        console->error(msg);
#else
        ion::Lazy issue(ERS_HERE, msg);
        ers::error(issue);
#endif
        return 1;
      }
      std::vector<std::string> tmp = result["addresses"].as<std::vector<std::string>>();

      const std::string msg1 = "Reading the addresses to write to";
      const auto msg2 = fmt::format("Got a list with {} entries", tmp.size());
#ifdef USE_SPDLOG
      console->debug(msg1);
      console->debug(msg2);
#else
      ERS_DEBUG(1, msg1);
      ERS_DEBUG(1, msg2);
#endif

      if (tmp.size() % 3 != 0) {
        const std::string msg = "You must define triplets of values (Address, length, value)";
#ifdef USE_SPDLOG
        console->error(msg);
#else
        ion::Lazy issue(ERS_HERE, msg);
        ers::error(issue);
#endif
        std::cout << options.help() << std::endl;
        return 1;
      }

      for (size_t i = 0; i < tmp.size() / 3; ++i) {
        const auto msg = fmt::format("Going through set {}", i);
#ifdef USE_SPDLOG
        console->debug(msg);
#else
        ERS_DEBUG(1, msg);
#endif
        uint8_t value = std::stoul(tmp[i * 3 + 2], 0, 16);
        std::vector<uint8_t> values;

        for (size_t j = 0; j < std::stoul(tmp[i * 3 + 1], 0, 10); ++j)
          values.push_back(value);

        addrValue[std::stoul(tmp[i * 3], 0, 10)] = values;
      }
    }

    if (read == false && (configFileName == "" && addrValue.size() == 0)) {
      const std::string msg = "You must either read or write to the GBTx";
#ifdef USE_SPDLOG
      console->error(msg);
#else
      ion::Lazy issue(ERS_HERE, msg);
      ers::error(issue);
#endif
      std::cout << options.help() << std::endl;
      return 1;
    }
  } catch (...) {
    std::cout << options.help() << std::endl;
    return 1;
  }

#ifdef USE_SPDLOG
  console->debug("The felix hostname is: {:s}", felixHostname);
  console->debug("The port to the GBTx is: {:d}", portToGBTx);
  console->debug("The port from the GBTx is: {:d}", portFromGBTx);
  console->debug("The e-link ID is: {:#04x}", elinkId);

#else
  ERS_DEBUG(1, "The felix hostname is: " << felixHostname);
  ERS_DEBUG(1, "The port to the GBTx is: " << portToGBTx);
  ERS_DEBUG(1, "The port from the GBTx is: " << portFromGBTx);
  ERS_DEBUG(1, "The e-link ID is: " << elinkId);
#endif

  try {
    IChandler var(felixHostname, portToGBTx, portFromGBTx, elinkId);

    if (read) {
      if (readAddresses.size() == 0) {
        const std::string msg = "Reading the full configuration";
#ifdef USE_SPDLOG
        console->debug(msg);
#else
        ERS_DEBUG(1, msg);
#endif
        auto cfg = var.readCfg();

        std::string tmp;
        for (size_t i = 0; i < cfg.size(); ++i) {
          if (i % 16 == 0) {
            if (i != 0) {
              std::cout << tmp << std::endl;
            }
            tmp = fmt::format("{:>4d}: ", i);
          }

          tmp += fmt::format(" {:02x}", cfg[i]);
        }
        std::cout << tmp << '\n';
      } else {
        const std::string msg = "Reading the specific register addresses";
#ifdef USE_SPDLOG
        console->debug(msg);
#else
        ERS_DEBUG(1, msg);
#endif
        auto regs = var.readRegs(readAddresses);

        for (auto& addr : regs) {
          std::cout << fmt::format("{:>4d}: {:02x}\n", addr.first, addr.second);
        }
      }
    } else {
      if (configFileName != "") {
        const auto msg = fmt::format("The config file is: ", configFileName);
#ifdef USE_SPDLOG
        console->debug(msg);
#else
        ERS_DEBUG(1, msg);
#endif
        var.sendCfg(configFileName);
      } else {
#ifdef USE_SPDLOG
        console->debug("Writing to the following sets of addresses:");
#else
        ERS_DEBUG(1, "Writing to the following sets of addresses:");
#endif
        for (auto& entry : addrValue) {
          std::string tmp = fmt::format("   - {}:", entry.first);
          for (auto& value : entry.second) {
            tmp += fmt::format(" {:x}", value);
          }
#ifdef USE_SPDLOG
          console->debug(tmp);
#else
          ERS_DEBUG(1, tmp);
#endif
        }

        var.sendRegs(addrValue);
      }
    }
  } catch (const std::runtime_error& e) {
    const auto errmsg = e.what();
#ifdef USE_SPDLOG
    console->debug(errmsg);
#else
    ERS_DEBUG(1, errmsg);
#endif
    return 20;
  }

  return 0;
}
