#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <fstream>
#include <exception>
#include <sstream>
#include <cstdio>
#include <string>

#include <fmt/format.h>

#include "ic-over-netio/IChandler.h"

#ifndef IC_STANDALONE_BUILD
ERS_DECLARE_ISSUE(nsw,
                  wgbtxIssue,
                  message,
                  ((std::string)message)
                  )
#endif

using namespace std;

string getPrintableGbtxConfig(const vector<uint8_t>& data){
    stringstream ss;
    ss<<"reg |";
    for (size_t i=0; i<data.size(); i++){
        if (i%16==0) {
            ss<<fmt::format("\n{:3d} |", i);
        }
        ss<<fmt::format(" {:02x}", data.at(i));
    }
    ss<<'\n';
    return ss.str();
}


vector<uint8_t> getPhases(const vector<uint8_t>& config) {
    // Parse full GBTx config, return phases
    // See page 199 and bits named "phaseSelectOutGroup*"
    // Each channel phase is 4 bits long, so two channels per byte
    // Register range is (Groups 0-6: 399-426, not used) (Groups 0-4: 399-418, used)
    // The group and channel is arranged sequentially:
        // for group 0, register 399: [ch1b0,ch1b1,ch1b2,ch1b3, ch0b0,ch0b1,ch0b2,ch0b3 ]
        // for group 0, register 400: [ch3b0,ch3b1,ch3b2,ch3b3, ch2b0,ch2b1,ch2b2,ch2b3 ]
        // for group 0, register 401: [ch5b0,ch5b1,ch5b2,ch5b3, ch4b0,ch4b1,ch4b2,ch4b3 ]
        // for group 0, register 402: [ch7b0,ch7b1,ch7b2,ch7b3, ch6b0,ch6b1,ch6b2,ch6b3 ]
        // For group 1, register 403: ...
    vector<uint8_t> phases;
    for (size_t i=399; i<426+1; i++){
    // for (size_t i=399; i<418; i++){
        int chanA = config.at(i)%16; // last 4 bits
        int chanB = config.at(i)/16; // first 4 bits
        phases.push_back(chanA); // check the order
        phases.push_back(chanB);
    }
    return phases;
}

void writePhases(const vector<uint8_t>& config, const string& outputPath) {
    // Save phases to a file
    cout<<"Writing to file: "<<outputPath<<'\n';
    ofstream ofile(outputPath,ios::out | ios::binary);
    if (ofile.is_open()){
        const vector<uint8_t> phases = getPhases(config);
        ofile.write(reinterpret_cast<const char*>(phases.data()),phases.size());
    }
    else{
        cout<<"ERROR: Output file not written"<<'\n';
    }
}


void printPhases(const vector<uint8_t>& config) {
    // Print out GBTx phases from configuration
    const vector<uint8_t> phases = getPhases(config);
    stringstream ss;
    ss<<"\n[GBTx Phases]\n";
    ss<<"Channel   0   1   2   3   4   5   6   7\n";
    for (size_t i=0; i<phases.size(); i++){
        if (i%8==0) ss<<"Group "<<i/8;
        ss << fmt::format(" | {:x}", phases.at(i));
        if ((i-7)%8==0) ss<<'\n';
    }
    cout<<ss.str()<<'\n';
}

void sendConfig(IChandler& ich, vector<uint8_t>& config){
    // Safely send config with IChandler
    try{
        ich.sendCfg(config);
    }
    catch(const std::exception& e) {
        cout<<e.what();
        nsw::wgbtxIssue issue(ERS_HERE, "Error sending configuration to GBTx. This could be because the link is not aligned, or because the FELIX server crashed, or the elink/port numbers are incorrect");
        ers::error(issue);
        throw issue;
    }
}

vector<uint8_t> readConfig(IChandler &ich){
    // read gbtx config and return vector
    vector<uint8_t> config;
    try{
        config = ich.readCfg();
    }
    catch(const std::exception& e) {
        cout<<e.what();
        nsw::wgbtxIssue issue(ERS_HERE, "Error reading configuration to GBTx. This could be because the link is not aligned, or because the FELIX server crashed, or the elink/port numbers are incorrect");
        ers::error(issue);
        throw issue;
    }

    cout<<"\n[Configuration read back]\n";
    cout<<"Size: "<<config.size()<<'\n';
    if (config.size()!=436)
        cout<<"Error: wrong size read back\n";
    cout<<getPrintableGbtxConfig(config);
    printPhases(config);
    return config;
}

vector<uint8_t> readFile(const string& ipath){
    // read input file and return configuration vector
    vector<uint8_t> ret;
    ifstream ifile (ipath.c_str());
    if (ifile.is_open()){
        string line;
        while ( getline (ifile,line) ) {
            ret.push_back(stoi(line,0,16));
        }
        ifile.close();
    }
    else {
        cout<<"\n\nERROR: Issue opening file\n\n";
    }
    return ret;
}

void setTrainingRegisters(vector<uint8_t> &config){
    // Set training registers on in config
    const vector<size_t> paTrainGroup0 = {78,79,80};
    const vector<size_t> paTrainGroup1 = {102,103,104};
    const vector<size_t> paTrainGroup4 = {174,175,176};
    const vector<size_t> paTrainGroup2 = {126,127,128};
    const vector<size_t> paTrainGroup3 = {150,151,152};
    const vector<size_t> paTrainGroup5 = {198,199,200};
    const vector<size_t> paTrainGroup6 = {222,223,224};


    // // # guesses
    // for (const auto reg : paTrainGroup0) config.at(reg) = 0xFF;
    // for (const auto reg : paTrainGroup1) config.at(reg) = 0b01110111;
    // for (const auto reg : paTrainGroup2) config.at(reg) = 0b00010001;
    // for (const auto reg : paTrainGroup3) config.at(reg) = 0b00010001;
    // for (const auto reg : paTrainGroup4) config.at(reg) = 0b00010001;
    // for (const auto reg : paTrainGroup5) config.at(reg) = 0x00;
    // for (const auto reg : paTrainGroup6) config.at(reg) = 0x00;

    // # guesses
    for (const auto reg : paTrainGroup0) config.at(reg) = 0xFF;
    for (const auto reg : paTrainGroup1) config.at(reg) = 0xFF;
    for (const auto reg : paTrainGroup2) config.at(reg) = 0xFF;
    for (const auto reg : paTrainGroup3) config.at(reg) = 0xFF;
    for (const auto reg : paTrainGroup4) config.at(reg) = 0xFF;
    for (const auto reg : paTrainGroup5) config.at(reg) = 0x00;
    for (const auto reg : paTrainGroup6) config.at(reg) = 0x00;

    // set automatic phase tracking mode 0b101010
    config.at(62) = 0x15;
}

void unsetTrainingRegisters(vector<uint8_t> &config){
    // Unset training registers on in config
    const vector<size_t> paTrainGroup0 = {78,79,80};
    const vector<size_t> paTrainGroup1 = {102,103,104};
    const vector<size_t> paTrainGroup2 = {126,127,128};
    const vector<size_t> paTrainGroup3 = {150,151,152};
    const vector<size_t> paTrainGroup4 = {174,175,176};
    const vector<size_t> paTrainGroup5 = {198,199,200};
    const vector<size_t> paTrainGroup6 = {222,223,224};

    for (const auto reg : paTrainGroup0) config.at(reg) = 0x00;
    for (const auto reg : paTrainGroup1) config.at(reg) = 0x00;
    for (const auto reg : paTrainGroup2) config.at(reg) = 0x00;
    for (const auto reg : paTrainGroup3) config.at(reg) = 0x00;
    for (const auto reg : paTrainGroup4) config.at(reg) = 0x00;
    for (const auto reg : paTrainGroup5) config.at(reg) = 0x00;
    for (const auto reg : paTrainGroup6) config.at(reg) = 0x00;

}

void setResetRegisters(vector<uint8_t> &config){
    // Set reset channel registers as performed in start of 191 training

    const vector<size_t> paResetGroup0 = {84,85,86};
    const vector<size_t> paResetGroup1 = {108,109,110};
    const vector<size_t> paResetGroup2 = {132,133,134};
    const vector<size_t> paResetGroup3 = {156,157,158};
    const vector<size_t> paResetGroup4 = {180,181,182};
    const vector<size_t> paResetGroup5 = {204,205,206};
    const vector<size_t> paResetGroup6 = {228,229,230};

    for (const auto reg : paResetGroup0) config.at(reg) = 0x00;
    for (const auto reg : paResetGroup1) config.at(reg) = 0xFF;
    for (const auto reg : paResetGroup2) config.at(reg) = 0xFF;
    for (const auto reg : paResetGroup3) config.at(reg) = 0xFF;
    for (const auto reg : paResetGroup4) config.at(reg) = 0xFF;
    for (const auto reg : paResetGroup5) config.at(reg) = 0x00; // Group 5 starts without reset
    for (const auto reg : paResetGroup6) config.at(reg) = 0x00; // Group 6 starts without reset

}

void unsetResetRegisters(vector<uint8_t> &config){
    // Unset reset channel registers as performed in end of 191 training

    const vector<size_t> paResetGroup0 = {84,85,86};
    const vector<size_t> paResetGroup1 = {108,109,110};
    const vector<size_t> paResetGroup2 = {132,133,134};
    const vector<size_t> paResetGroup3 = {156,157,158};
    const vector<size_t> paResetGroup4 = {180,181,182};
    const vector<size_t> paResetGroup5 = {204,205,206};
    const vector<size_t> paResetGroup6 = {228,229,230};

    for (const auto reg : paResetGroup0) config.at(reg) = 0x00;
    for (const auto reg : paResetGroup1) config.at(reg) = 0x01; // Group 1 channel 0 kept in reset (SCA ADDC)
    for (const auto reg : paResetGroup2) config.at(reg) = 0x00;
    for (const auto reg : paResetGroup3) config.at(reg) = 0x00;
    for (const auto reg : paResetGroup4) config.at(reg) = 0x00;
    for (const auto reg : paResetGroup5) config.at(reg) = 0x00;
    for (const auto reg : paResetGroup6) config.at(reg) = 0x00;

}

int main (int argc, char** argv)
{

    // required settings
    string ipath="none";
    string felixServerIp="none";
    string opath = "none";
    int portToGBTx=-1;
    int portFromGBTx=-1;
    int elinkId=-1;
    int sleepTime=1;

    // Check if reading or writing or training
    string mode="none";
    for (size_t i = 0; i < argc; i++){
        if      (!strcmp(argv[i],"-t")) mode="train";
        else if (!strcmp(argv[i],"-w")) mode="write";
        else if (!strcmp(argv[i],"-r")) mode="read";
        else if (!strcmp(argv[i],"-x")) mode="reset";
        else if (!strcmp(argv[i],"-s")) mode="scramble";
        else if (!strcmp(argv[i],"-h")) mode="help";
    }
    // get required settings

    for (size_t i = 0; i < argc; i++){
        if (!strcmp(argv[i],"--ip")) felixServerIp=argv[i+1];
        if (!strcmp(argv[i],"--iport")) portToGBTx=atoi(argv[i+1]);
        if (!strcmp(argv[i],"--oport")) portFromGBTx=atoi(argv[i+1]);
        if (!strcmp(argv[i],"--elink")) elinkId=atoi(argv[i+1]);
        if (!strcmp(argv[i],"--sleep")) sleepTime=atoi(argv[i+1]);
        if (!strcmp(argv[i],"-c")) ipath=argv[i+1];
        if (!strcmp(argv[i],"-o")) opath=argv[i+1];
    }

    // check inputs
    if (mode=="none"){
        cout<<"Please run with -r,-w,-t,-x, or -h for help\n";
        cout<<"Example: --ip pcatlnswfelix10.cern.ch --iport 12340 --oport 12350 --elink 62\n";
        return 0;
    }

    if (mode=="help"){
        cout<<"NAME\n   wgbt - write to and read from GBTx through netio\n";
        cout<<"SYNOPSIS\n   wgbt [-r,-w,-t,-h]... --iport WWW --oport XXX --ip YYY --elink ZZZ \n";
        cout<<"-r      | Read configuration from GBTx\n";
        cout<<"-w      | Write configuration to GBTx from file. Include configuration file with -c. The file should be line separated hex bytes\n";
        cout<<"-t      | Train GBTx e-links\n";
        cout<<"-x      | Set PA Reset Registers\n";
        cout<<"-s      | Scramble PA registers, for debugging purpose\n";
        cout<<"--iport | Input port for GBTx. For example, 12340\n";
        cout<<"--oport | Output port for GBTx. For example, 12350\n";
        cout<<"--ip    | Address for FELIX machine. For example, pcatlnswfelix10.cern.ch\n";
        cout<<"--elink | FELIX e-link number in decimal for GBTx. For example 62 if you want to configure link 0x3E\n";
        cout<<"--sleep | Optional: number of seconds to wait for training\n";
        cout<<"\nExample uses:\n";
        cout<<"wgbt -w --ip pcatlnswfelix10.cern.ch --iport 12340 --oport 12350 --elink 62 -c config.txt\n";
        cout<<"wgbt -x --ip pcatlnswfelix10.cern.ch --iport 12340 --oport 12350 --elink 62\n";
        cout<<"wgbt -t --ip pcatlnswfelix10.cern.ch --iport 12340 --oport 12350 --elink 62\n";
        cout<<"wgbt -r --ip pcatlnswfelix10.cern.ch --iport 12340 --oport 12350 --elink 62\n";
        cout<<'\n';
        return 0;
    }

    if (felixServerIp=="none"||portToGBTx==-1||portFromGBTx==-1||elinkId==-1){
        cout<<"Please set required inputs.\nExample: wgbt --ip pcatlnswfelix10.cern.ch --iport 12340 --oport 12350 --elink 62\n";
        return 0;
    }


    cout<<"##################################################\n";
    cout<<"# Mode:"<<mode.c_str()<<'\n';
    cout<<"# felixServerIp:  "<<felixServerIp<<'\n';
    cout<<"# portToGBTx:   "<<portToGBTx<<'\n';
    cout<<"# portFromGBTx: "<<portFromGBTx<<'\n';
    cout<<"# elinkId:      "<<elinkId<<'\n';
    cout<<"##################################################\n";

    cout<<"Making IChandler\n";
    IChandler ich(felixServerIp, portToGBTx, portFromGBTx, elinkId);

    if (mode=="train"){
        if (ipath!="none"){
            cout<<"You can't do the training and also specify a configuration path with -c FILENAME\n";
            return 0;
        }
        // read config
        vector<uint8_t> config = readConfig(ich);

        // set reset registers
        cout<<"==> 1) Setting reset registers\n";
        setResetRegisters(config);
        // sendConfig(ich,config); // send intermediate steps (seems not needed)
        // sleep(sleepTime);

        // set training registers
        setTrainingRegisters(config);
        cout<<"==> 2) Setting training registers\n";
        sendConfig(ich,config);
        sleep(sleepTime);

        // unset training registers
        cout<<"==> 3) Unsetting training registers\n";
        unsetTrainingRegisters(config);
        // sendConfig(ich,config); // send intermediate steps (seems not needed)
        // sleep(sleepTime);

        // unset reset registers
        cout<<"==> 4) Unsetting reset registers\n";
        unsetResetRegisters(config);
        sendConfig(ich,config);

        cout<<"Configuration after training complete\n";
        config = readConfig(ich);

        // save phases
        if (opath!="none"){
            writePhases(config,opath);
        }

    }
    else if (mode=="write"){
        if (ipath=="none"){
            cout<<"Please specify input configuration file with -c FILENAME\n";
            return 0;
        }
        // read input file
        vector<uint8_t> config = readFile(ipath);
        cout<<"About to upload this configuration:\n";
        cout<<getPrintableGbtxConfig(config);
        cout<<"Uploading...\n";
        sendConfig(ich,config);
        cout<<"Config read back after uploading:\n";
        readConfig(ich);
    }
    else if (mode=="read"){
        cout<<"Reading...\n";
        if (ipath!="none") cout<<"Warning: you set a -c config path, but you also set the -r read flag\n";
        vector<uint8_t> config = readConfig(ich);
        // Write output phases to file (testing)
        if (opath!="none"){
            writePhases(config,opath);
        }
    }
    else if (mode=="reset"){
        cout<<"Putting PA reset registers in 191 reset state...\n";
        vector<uint8_t> config = readConfig(ich);
        unsetResetRegisters(config);
        sendConfig(ich,config);
        cout<<"Config read back after resetting PA reset registers in config:\n";
        readConfig(ich);
    }
    else if (mode=="scramble"){
        cout<<"Scrambling phases, just for testing...\n";
        const vector<int> pa = {67,71,75, 187,191,195, 186,190,194, 186,190,194, 189,193,197, 187,191,195, 188,192,196, 116,120,124, 116,120,124, 117,121,125, 117,121,125, 114,118,122, 114,118,122, 115,119,123, 115,119,123, 233,237,241, 163,167,171, 163,167,171, 162,166,170, 162,166,170, 165,169,173, 165,169,173, 164,168,172, 164,168,172, 210,214,218, 210,214,218, 211,215,219, 212,216,220, 212,216,220, 213,217,221, 213,217,221, 189,193,197, 93,97,101, 93,97,101, 92,96,100, 92,96,100, 91,95,99, 91,95,99, 90,94,98, 90,94,98, 211,215,219, 141,145,149, 188,192,196, 140,144,148, 140,144,148, 141,145,149, 138,142,146, 138,142,146, 139,143,147, 139,143,147, 69,73,77, 69,73,77, 68,72,76, 68,72,76, 67,71,75, 66,70,74, 66,70,74};
        vector<uint8_t> config = readConfig(ich);
        for (const auto pav : pa) config.at(pav)=0xFF;
        // set static phase selection mode 0b000000
        config.at(62) = 0x00;
        cout<<"Sending following scrambled config\n";
        cout<<getPrintableGbtxConfig(config);
        sendConfig(ich,config);
        cout<<"Config read back after uploading scrambled config:\n";
        readConfig(ich);
    }

    return 0;
}

