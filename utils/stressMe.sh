#!/bin/env sh
# SCRIPT="/eos/home-r/rowang/public/sw/atlas-tdaq-felix/software/x86_64-centos7-gcc8-opt/ic-over-netio/multi_gbtx_ic"
SCRIPT="/eos/home-r/rowang/public/sw/nswdaq/x86_64-centos7-gcc8-opt/ic-over-netio/multi_gbtx_ic"
DIRNAME=`dirname $BASH_SOURCE`
for index in {1..2500}
do
    start=$SECONDS
    echo "RETRY: $index"
    if [[ $1 == bb5 ]] || [[ $1 == vs ]] || [[ $1 == 191_MM ]] || [[ $1 == 191_sTGC ]] || [[ $1 == 180 ]]; then
        # TODO what is trace?
        if [[ $2 == debug ]]; then
            $SCRIPT -c $DIRNAME/../cfg_${1}.json -d
        elif  [[ $2 == trace ]]; then
            $SCRIPT -c $DIRNAME/../cfg_${1}.json -t
        else
            $SCRIPT -c $DIRNAME/../cfg_${1}.json
        fi
        if [[ $? == 20 ]]; then
            break
        fi
        echo "time spent :" $(($SECONDS-$start))
    else
        echo "please pass argument bb5 or vs, and then choose if you want debug or not E.g. : $ source stressMe.sh bb5 debug or $ source stressMe.sh bb5"
        break
    fi
    sleep 0.2
done
