# - Try to find FELIX libraries
#
# Once done this will define
#  FELIX_FOUND - system has FELIX software stack available
#  FELIX_PATH - the FELIX release directory
#  FELIX_INCLUDE_DIR - the FELIX include directory
#  FELIX_LIB_DIR - the FELIX lib directory
#  target felix::felix
#
#  FELIXBUS_LIBRARIES - libfelixbus
#  target felix::felixbus
#
#  NETIO_LIBRARIES - libnetio
#  target felix::netio

set(FELIX_DIR "" CACHE INTERNAL "User provided location for a FELIX installation to compile/link against")
set(FELIX_RELEASE_VERSION "felix-04-02-00-b9-rm4" CACHE INTERNAL "FELIX release version to compile against")

## Maybe want to be able to select FELIX version and compiler profile here?
## But this doesn't exist for RPM builds, and not all compiler
## profiles are available with FELIX releases, so need a fallback

## FELIX is only compiled for the -opt profile, so if we can find a
## matching -opt profile when we compile with -dbg, use that
if(DEFINED ENV{CMTCONFIG})
    string(REGEX REPLACE "-dbg$" "-opt" FELIX_COMPILER_PROFILE $ENV{CMTCONFIG})
else()
    if(CMTCONFIG)
        string(REGEX REPLACE "-dbg$" "-opt" FELIX_COMPILER_PROFILE ${CMTCONFIG})
    else()
        set(FELIX_COMPILER_PROFILE "x86_64-centos7-gcc11-opt")
    endif()
endif()

set(FELIX_SEARCH_PATHS
    /cvmfs/atlas-online-nightlies.cern.ch/felix/releases/${FELIX_RELEASE_VERSION}-stand-alone/${FELIX_COMPILER_PROFILE} # GPN
    /sw/atlas/felix/${FELIX_RELEASE_VERSION}-stand-alone/${FELIX_COMPILER_PROFILE} # P1
    /opt/felix # RPM
    )

message(STATUS "Looking for FELIX dependencies in " ${FELIX_DIR} ${FELIX_SEARCH_PATHS})

## In case we reconfigure with a different version, make sure to use
## that instead of the previously cached values
unset(FELIX_VERSION_EXECUTABLE CACHE)
unset(FELIX_LIB_DIR CACHE)
unset(FELIX_INCLUDE_DIR CACHE)
unset(FELIX_PATH CACHE)

# Find felix-version application (to extract version info)
find_program(FELIX_VERSION_EXECUTABLE
  NAMES felix-fid
  HINTS ${FELIX_DIR}
  PATHS ${FELIX_SEARCH_PATHS}
  PATH_SUFFIXES bin
 )

find_path(FELIX_LIB_DIR
  NAMES libfelix.so
  HINTS ${FELIX_DIR}
  PATHS ${FELIX_SEARCH_PATHS}
  PATH_SUFFIXES lib
 )

find_path(FELIX_INCLUDE_DIR
  NAMES felixbase/common.hpp
  HINTS ${FELIX_DIR}
  PATHS ${FELIX_SEARCH_PATHS}
  PATH_SUFFIXES include/felixbase include
 )

get_filename_component(FELIX_PATH "${FELIX_INCLUDE_DIR}" DIRECTORY CACHE)

set(FELIX_LIBRARY_COMPONENTS felix felix-client-lib felix-client-thread felixbase felixbus regmap packetformat netio fabric)
set(FELIX_SEARCH_COMPONENTS ${FELIX_HEADER_COMPONENTS} ${FELIX_LIBRARY_COMPONENTS})

## Need to find TBB for netio
find_package(TBB REQUIRED)

if(NOT TARGET tbb)
  if(TARGET TBB::tbb)
    add_library(tbb ALIAS TBB::tbb)
  elseif(TARGET TBB)
    add_library(tbb ALIAS TBB)
  elseif(TBB_ROOT_DIR)
    ## Fell into the TDAQ FindTBB trap, create an actual target...
    add_library(tbb UNKNOWN IMPORTED GLOBAL)
    set_target_properties(tbb
      PROPERTIES
        IMPORTED_LOCATION ${TBB_LIBRARY}
        INTERFACE_INCLUDE_DIRECTORIES "${TBB_INCLUDE_DIR}"
      )
  else()
    message(ERROR "Unable to find TBB target")
  endif()
endif()

foreach(_felix_component ${FELIX_SEARCH_COMPONENTS})
    unset(FELIX_${_felix_component}_LIBRARY CACHE)
    if(${_felix_component} STREQUAL "fabric")
      set(_felix_component_lib_name "lib${_felix_component}.so.1")
    else()
      set(_felix_component_lib_name ${_felix_component})
    endif()
    find_library(FELIX_${_felix_component}_LIBRARY
        NAMES ${_felix_component_lib_name}
        HINTS ${FELIX_DIR}
        PATHS ${FELIX_SEARCH_PATHS}
        PATH_SUFFIXES lib
    )

    if(FELIX_${_felix_component}_LIBRARY)
        list(APPEND FELIX_LIBRARIES "${FELIX_${_felix_component}_LIBRARY}")
        list(APPEND FELIX_FOUND_COMPONENTS "${_felix_component}")
    endif()

    if(FELIX_${_felix_component}_LIBRARY AND EXISTS "${FELIX_${_felix_component}_LIBRARY}")
        set(FELIX_${_felix_component}_FOUND TRUE)
    else()
        set(FELIX_${_felix_component}_FOUND FALSE)
    endif()

    mark_as_advanced(FELIX_${_felix_component}_LIBRARY)
endforeach()


if(FELIX_VERSION_EXECUTABLE)
  message(VERBOSE "Extracting FELIX version from ${FELIX_VERSION_EXECUTABLE}")
  ## felix-fid --version returns the path of the executable *not* the version...
  ## So we look for felix-XX-YY-ZZ(-(b|rc)AA)?-stand
  ## N.B. The regex will *not* work if using an unversioned release tree
  ## Also, because CMake regex syntax doesn't allow this compact notation...
  # set(FELIX_VERSION_REGEX "^felix(master|(-[0-9]{2}){3}(-(b|rc)[0-9]+)?)-stand-alone$")
  set(FELIX_VERSION_REGEX "felix(-[0-9][0-9]-[0-9][0-9]-[0-9][0-9])(-(b|rc)[0-9]+)?-stand-")

  execute_process(COMMAND ${FELIX_VERSION_EXECUTABLE} --version OUTPUT_VARIABLE FELIX_VERSION_OUTPUT)
  string(REGEX MATCHALL "${FELIX_VERSION_REGEX}" FELIX_VERSION_LIST "${FELIX_VERSION_OUTPUT}")
  if(NOT FELIX_VERSION_LIST)
    ## Fill with a dummy version if we detect an unversioned release tree
    set(FELIX_VERSION_LIST "felix-99-99-99-unknown")
  endif()

  string(REPLACE "-" ";" VERSION_LIST ${FELIX_VERSION_LIST})
  list(GET VERSION_LIST 1 FELIX_VERSION_MAJOR)
  list(GET VERSION_LIST 2 FELIX_VERSION_MINOR)
  list(GET VERSION_LIST 3 FELIX_VERSION_PATCH)
  list(GET VERSION_LIST 4 FELIX_VERSION_PREREL)

  string(REGEX REPLACE "^[0]?([0-9]+)[^0-9]*$" "\\1" FELIX_VERSION_PREREL ${FELIX_VERSION_PREREL})
  string(REGEX REPLACE "^[0]?([0-9]+)[^0-9]*$" "\\1" FELIX_VERSION_PATCH ${FELIX_VERSION_PATCH})
  string(REGEX REPLACE "^[0]?([0-9]+)[^0-9]*$" "\\1" FELIX_VERSION_MINOR ${FELIX_VERSION_MINOR})
  string(REGEX REPLACE "^[0]?([0-9]+)[^0-9]*$" "\\1" FELIX_VERSION_MAJOR ${FELIX_VERSION_MAJOR})

  set(FELIX_VERSION "${FELIX_VERSION_MAJOR}.${FELIX_VERSION_MINOR}.${FELIX_VERSION_PATCH}")
  set(FELIX_FIND_VERSION_COMPLETE ${FELIX_VERSION})

  ## This may be too strict, but for now, if the version is not an
  ## exact match to that specified, it is marked as incompatible
  if(FELIX_FIND_VERSION)
    if(${FELIX_VERSION} VERSION_EQUAL ${FELIX_FIND_VERSION})
      set(FELIX_VERSION_EXACT TRUE)
      set(FELIX_VERSION_UNSUITABLE FALSE)
      set(FELIX_VERSION_COMPATIBLE TRUE)
    else()
      set(FELIX_VERSION_EXACT FALSE)
      set(FELIX_VERSION_UNSUITABLE TRUE)
      set(FELIX_VERSION_COMPATIBLE FALSE)
    endif()
  endif()
endif()

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(FELIX
  FOUND_VAR FELIX_FOUND
  REQUIRED_VARS
    FELIX_PATH
    FELIX_INCLUDE_DIR
    FELIX_LIB_DIR
  HANDLE_COMPONENTS
  VERSION_VAR FELIX_VERSION
)

if(FELIX_FOUND)
  if(NOT FELIX_FIND_QUIETLY)
    message(STATUS "FELIX version: " ${FELIX_VERSION})

    if(FELIX_FOUND_COMPONENTS)
      message(STATUS "Found the following FELIX components:")
      foreach(_felix_component IN LISTS FELIX_FOUND_COMPONENTS)
        message(STATUS "  " ${_felix_component})
      endforeach()
    endif()
    set(FELIX_FOUND_COMPONENTS)
  endif()

  get_filename_component(FELIX_PREFIX "${FELIX_INCLUDE_DIR}" PATH)

  if(FELIX_felix_LIBRARY AND NOT TARGET felix::felix)
      add_library(felix::felix UNKNOWN IMPORTED GLOBAL)
      set_target_properties(felix::felix
        PROPERTIES
          IMPORTED_LOCATION ${FELIX_felix_LIBRARY}
          INTERFACE_LINK_DIRECTORIES "${FELIX_LIB_DIR}"
          INTERFACE_INCLUDE_DIRECTORIES "${FELIX_INCLUDE_DIR}"
      )
  endif()

  if(FELIX_fabric_LIBRARY AND NOT TARGET felix::fabric)
      add_library(felix::fabric UNKNOWN IMPORTED GLOBAL)
      set_target_properties(felix::fabric
        PROPERTIES
          IMPORTED_LOCATION ${FELIX_fabric_LIBRARY}
          INTERFACE_LINK_DIRECTORIES "${FELIX_LIB_DIR}"
          INTERFACE_INCLUDE_DIRECTORIES "${FELIX_INCLUDE_DIR}"
      )
  endif()

  if(FELIX_packetformat_LIBRARY AND NOT TARGET felix::packetformat)
      add_library(felix::packetformat UNKNOWN IMPORTED GLOBAL)
      set_target_properties(felix::packetformat
        PROPERTIES
          IMPORTED_LOCATION ${FELIX_packetformat_LIBRARY}
          INTERFACE_LINK_DIRECTORIES "${FELIX_LIB_DIR}"
          INTERFACE_INCLUDE_DIRECTORIES "${FELIX_INCLUDE_DIR}"
      )
  endif()

  if(FELIX_regmap_LIBRARY AND NOT TARGET felix::regmap)
      add_library(felix::regmap UNKNOWN IMPORTED GLOBAL)
      set_target_properties(felix::regmap
        PROPERTIES
          IMPORTED_LOCATION ${FELIX_regmap_LIBRARY}
          INTERFACE_LINK_DIRECTORIES "${FELIX_LIB_DIR}"
          INTERFACE_INCLUDE_DIRECTORIES "${FELIX_INCLUDE_DIR}"
      )
  endif()

  if(FELIX_felixbase_LIBRARY AND NOT TARGET felix::felixbase)
      add_library(felix::felixbase UNKNOWN IMPORTED GLOBAL)
      set_target_properties(felix::felixbase
        PROPERTIES
          IMPORTED_LOCATION ${FELIX_felixbase_LIBRARY}
          INTERFACE_INCLUDE_DIRECTORIES "${FELIX_INCLUDE_DIR}"
          INTERFACE_LINK_DIRECTORIES "${FELIX_LIB_DIR}"
	  INTERFACE_LINK_LIBRARIES "packetformat"
      )
  endif()

  if(FELIX_felixbus_LIBRARY AND NOT TARGET felix::felixbus)
      add_library(felix::felixbus UNKNOWN IMPORTED GLOBAL)
      set_target_properties(felix::felixbus
        PROPERTIES
          IMPORTED_LOCATION ${FELIX_felixbus_LIBRARY}
          INTERFACE_INCLUDE_DIRECTORIES "${FELIX_INCLUDE_DIR}"
          INTERFACE_LINK_DIRECTORIES "${FELIX_LIB_DIR}"
	  INTERFACE_LINK_LIBRARIES "zyre;czmq;zmq;sodium"
      )
  endif()

  if(FELIX_netio_LIBRARY AND NOT TARGET felix::netio)
      add_library(felix::netio UNKNOWN IMPORTED GLOBAL)
      set_target_properties(felix::netio
        PROPERTIES
          IMPORTED_LOCATION ${FELIX_netio_LIBRARY}
          INTERFACE_INCLUDE_DIRECTORIES "${FELIX_INCLUDE_DIR}"
          INTERFACE_LINK_DIRECTORIES "${FELIX_LIB_DIR}"
	  INTERFACE_LINK_LIBRARIES "rt;tbb;felix::fabric"
      )
  endif()

  ### Create the alias targets
  if(NOT TARGET felix)
      add_library(felix ALIAS felix::felix)
  endif()
  if(NOT TARGET packetformat)
      add_library(packetformat ALIAS felix::packetformat)
  endif()
  if(NOT TARGET regmap)
      add_library(regmap ALIAS felix::regmap)
  endif()
  if(NOT TARGET felixbase)
      add_library(felixbase ALIAS felix::felixbase)
  endif()
  if(NOT TARGET felixbus)
      add_library(felixbus ALIAS felix::felixbus)
  endif()
  if(NOT TARGET netio)
      add_library(netio ALIAS felix::netio)
  endif()
endif()

mark_as_advanced(FELIX_LIB_DIR FELIX_PATH FELIX_INCLUDE_DIR FELIX_LIBRARIES)

unset(FELIX_FOUND_COMPONENTS)
unset(FELIX_HEADER_COMPONENTS)
unset(FELIX_LIBRARY_COMPONENTS)
unset(FELIX_SEARCH_COMPONENTS)
unset(FELIX_VERSION_EXECUTABLE)
unset(FELIX_SEARCH_PATHS)
